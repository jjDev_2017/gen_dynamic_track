
int COLS, ROWS;
int SCALE = 20;
int mesh1_width = 600;
int mesh1_height = 600;

float x_offset = 0.0;
float y_offset = 0.0;

float[] X_VALUES = new float[600];
int SPACE_BETWEEN_MESHES = 200;

float INC = 0; 


void setup(){
  size(600, 600, P3D);
  COLS = mesh1_width / SCALE;
  ROWS = mesh1_height;
  
}


void draw(){
  
  background(81, 145, 247);
  stroke(0);
  //fill(0);
  //noFill();
  
  int left_mesh_width = ((width / 2) + SPACE_BETWEEN_MESHES) / SCALE;
  int right_mesh_origin = SPACE_BETWEEN_MESHES + 10;
  
  INC += 0.001;
  //int x_increment = 100 / SCALE; 
  //int y = 0;
  //float x_offset = 0;
  //float y_offset = 0;
  float last_x_value = 0.0;
  last_x_value = draw_mesh_1(0.0, 0.0, 50.0);
  
  
  last_x_value = draw_mesh_1(last_x_value, 50.0, 100.0);
  
  
  draw_mesh_2();
  delay(2000); //DEBUG
 
  
  
}

float draw_mesh_1(float x_start, float y_start, float y_end){
  beginShape();
  fill(0, 0, x_start * 10); //DEBUG color the other left mesh
  vertex(0, y_start);
  float y = 0;
  float x_result = 0.0;
  int x_increment = 100 / SCALE; 
  
  for(y = y_start; y <= y_end; y++){
    x_result = x_increment + (noise(x_offset, y_offset)* SCALE);
    x_result *= SCALE;
    vertex(x_result, y);
    X_VALUES[int(y)] = x_result;
    x_offset += 0.01;
    //y_offset += 0.01 + INC;
  }
  
  vertex(0, y_end);
  endShape();
  return x_result;
}

void draw_mesh_2(){
  beginShape();
  fill(0);
  vertex(600, 0);
  for(int j=1; j<100; j++){
    float x_val = X_VALUES[j];
    vertex(x_val + (2 * SCALE), j);
  }
  
  vertex(600, 101);
  endShape();
}


//float draw_mesh_2(float x_start, float y_start, float y_end, float x_offset, float y_offset){
//  beginShape();
//  vertex(x, y_start);
//  float y = 0;
//  float x_result = 0.0;
//  int x_increment = (100 / SCALE) + (SPACE_BETWEEN_MESHES / SCALE); 
  
//  for(y = y_start; y <= y_end; y++){
//    x_result = x_increment + (noise(x_offset, y_offset)* SCALE);
//    vertex(x_result * SCALE, y);
//    x_offset += 0.01;
//    //y_offset += 0.01 + INC;
//  }
  
//  vertex(600, y_end * SCALE);
//  endShape();
//  return x_result;
//}
  

//void draw_mesh(int start_x, int start_y){
//  float y_offset = start_y + Y_INC;
//  for(int x = start_x; x < COLS; x++){
//    float x_offset = start_x;
//    beginShape(TRIANGLE_STRIP);
//    for(int y = start_y; y < ROWS; y++){  
//      float x_result = x + (map(noise(x_offset, y_offset), 0, 1, -20, 20) * SCALE);
//      //point(x_result, y * SCALE);
      
//      vertex(x_result, y * SCALE);
//      vertex(x_result, (y+1) * SCALE);
//      //rect(x * SCALE, y_offset * SCALE, SCALE, SCALE);
//      x_offset += 0.2;
//    }
//    y_offset += 0.2;
//    endShape();
//  }
//}
